package com.hereisalexius.ppp;

import com.hereisalexius.ppp.parser.ProductInfoParser;
import com.hereisalexius.ppp.utils.AppConfig;
import com.hereisalexius.ppp.utils.FieldProperty;
import com.hereisalexius.ppp.utils.FieldSelectionSource;
import com.hereisalexius.ppp.utils.FieldSelectionStyle;
import com.hereisalexius.ppp.utils.ProductField;
import com.hereisalexius.ppp.utils.Result;
import java.util.concurrent.Executors;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ConfigApp extends Application {

    private BorderPane rootPane;
    private Scene scene;

    private TextField encodingField;
    private TextField userAgentField;
    private TextField timeoutField;
    private TextField pagerSelectorField;
    private TextField pagerLinkSelectorField;
    private TextField productLinkSelectorField;

    private ListView<ProductField> fieldsList;

    private TextField defaultValueField;
    private ComboBox<FieldSelectionStyle> selectionStyleComboBox;
    private ComboBox<FieldSelectionSource> selectionSourceComboBox;
    private TextField selectionCodesField;
    private TextField selectionDelimiterField;
    private TextField selectionPositiveField;
    private TextField selectionNegativeField;
    private TextField selectionAttributeField;
    private ComboBox<Result> selectionResultComboBox;
    private TextField selectionTableKeyField;

    private FieldProperty currentFieldProperty;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Config");
        initProperties();
        rootPane = new BorderPane();
        initComponents();
        scene = new Scene(rootPane, 640, 480);
        primaryStage.setScene(scene);
        load();
        primaryStage.show();
    }

    private void initProperties() {
        AppConfig.getInstance().preLoad();
    }

    private void initComponents() {
        rootPane.setPadding(new Insets(4, 4, 4, 4));
        initTop();
        initLeft();
        initCenter();
        initBottom();
    }

    private void initCenter() {
        VBox center = new VBox(4);
        center.setPadding(new Insets(4, 4, 4, 4));
        center.getChildren().addAll(new Label(" "), new Label(" "));

        defaultValueField = initTextField(center, "Default Value :");

        selectionStyleComboBox = new ComboBox<>();
        selectionStyleComboBox.setItems(FXCollections.observableArrayList(FieldSelectionStyle.values()));
        selectionStyleComboBox.getSelectionModel().selectedItemProperty().addListener((obs, o, n) -> {

            switch (n) {
                case SIMPLE_CSS_SELECTOR:
                    selectionSourceComboBox.setDisable(false);
                    selectionCodesField.setDisable(false);
                    selectionDelimiterField.setDisable(true);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(false);
                    selectionTableKeyField.setDisable(true);
                    break;
                case MULTIPLY_CSS_SELECTOR:
                    selectionSourceComboBox.setDisable(false);
                    selectionCodesField.setDisable(false);
                    selectionDelimiterField.setDisable(false);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(false);
                    selectionTableKeyField.setDisable(true);
                    break;
                case CONTAINS_TEXT:
                    selectionSourceComboBox.setDisable(false);
                    selectionCodesField.setDisable(false);
                    selectionDelimiterField.setDisable(true);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(false);
                    selectionTableKeyField.setDisable(true);
                    break;
                case BINARY_TABLE_CSS_SELECTOR:
                    selectionSourceComboBox.setDisable(false);
                    selectionCodesField.setDisable(false);
                    selectionDelimiterField.setDisable(true);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(false);
                    selectionTableKeyField.setDisable(true);
                    break;
                case VALUE_FROM_BINARY_TABLE_CSS_SELECTOR:
                    selectionSourceComboBox.setDisable(false);
                    selectionCodesField.setDisable(false);
                    selectionDelimiterField.setDisable(true);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(false);
                    selectionTableKeyField.setDisable(false);
                    break;
                default:
                    selectionSourceComboBox.setDisable(true);
                    selectionCodesField.setDisable(true);
                    selectionDelimiterField.setDisable(true);
                    selectionPositiveField.setDisable(true);
                    selectionNegativeField.setDisable(true);
                    selectionAttributeField.setDisable(true);
                    selectionResultComboBox.setDisable(true);
                    selectionTableKeyField.setDisable(true);
            }

        });
        initCombobox(center, "Selection Style :", selectionStyleComboBox);

        selectionSourceComboBox = new ComboBox<>();
        selectionSourceComboBox.setItems(FXCollections.observableArrayList(FieldSelectionSource.values()));
        selectionSourceComboBox.getSelectionModel().selectedItemProperty().addListener((obs, o, n) -> {
            if (n == FieldSelectionSource.ATTRIBUTE) {
                selectionAttributeField.setDisable(false);
            } else {
                selectionAttributeField.setDisable(true);
            }
        });
        initCombobox(center, "Selection Source :", selectionSourceComboBox);

        selectionCodesField = initTextField(center, "Codes to Find :");
        selectionDelimiterField = initTextField(center, "Delimiter :");
        selectionPositiveField = initTextField(center, "Positive Value :");
        selectionNegativeField = initTextField(center, "Negative Value :");
        selectionAttributeField = initTextField(center, "Attribute(html) :");

        selectionResultComboBox = new ComboBox<>();
        selectionResultComboBox.setItems(FXCollections.observableArrayList(Result.values()));
        initCombobox(center, "Result(in) :", selectionResultComboBox);

        selectionTableKeyField = initTextField(center, "Table Key :");
        center.setDisable(true);
        rootPane.setCenter(center);
    }

    private void initTop() {
        VBox top = new VBox(4);
        top.setPadding(new Insets(4, 4, 4, 4));

        encodingField = initTextField(top, "Encoding :");
        userAgentField = initTextField(top, "User Agent :");
        timeoutField = initTextField(top, "Timeout :");
        pagerSelectorField = initTextField(top, "Pager Css :");
        pagerLinkSelectorField = initTextField(top, "Pager Link Css :");
        productLinkSelectorField = initTextField(top, "Product Link Css :");
        rootPane.setTop(top);
    }

    private void initLeft() {
        VBox left = new VBox(4);
        left.setPadding(new Insets(4, 4, 4, 4));
        left.setAlignment(Pos.CENTER);
        fieldsList = new ListView<>();
        fieldsList.setItems(FXCollections.observableArrayList(ProductField.values()));
        fieldsList.getSelectionModel().selectedItemProperty().addListener((obs, o, n) -> {
            onFieldSwiched(n);
        });

        left.getChildren().addAll(new Label("Parsing fields:"), fieldsList);
        rootPane.setLeft(left);
    }

    private TextField initTextField(VBox center, String title) {
        HBox row = new HBox(4);
        row.setAlignment(Pos.CENTER);
        row.setPadding(new Insets(4, 4, 4, 4));
        TextField field = new TextField();
        HBox.setHgrow(field, Priority.ALWAYS);
        Label l = new Label(title);
        l.setPrefWidth(100);
        row.getChildren().addAll(l, field);
        center.getChildren().add(row);
        return field;
    }

    private ComboBox initCombobox(VBox center, String title, ComboBox box) {
        HBox row = new HBox(4);
        row.setAlignment(Pos.CENTER);
        row.setPadding(new Insets(4, 4, 4, 4));
        HBox.setHgrow(box, Priority.ALWAYS);
        box.setMaxSize(9999, 9999);
        Label l = new Label(title);
        l.setPrefWidth(100);
        row.getChildren().addAll(l, box);
        center.getChildren().add(row);

        return box;
    }

    private void initBottom() {
        HBox bottom = new HBox(4);
        bottom.setPadding(new Insets(4, 4, 4, 4));
        bottom.setAlignment(Pos.BASELINE_RIGHT);
        Button saveButton = new Button("Save");
        saveButton.setOnAction((e) -> {
            save();
        });
        Button factoryButton = new Button("Factory values");
        Button restoreButton = new Button("Restore");
        bottom.getChildren().addAll(saveButton);
        rootPane.setBottom(bottom);
    }

    private void onFieldSwiched(ProductField pf) {
        rootPane.getCenter().setDisable(false);
        saveFieldValues();
        loadFieldValues(pf);
    }

    private void loadFieldValues(ProductField pf) {
        currentFieldProperty = new FieldProperty(pf);

        String defaultValue = currentFieldProperty.getDefaultValue();
        if (defaultValue == null) {
            defaultValue = "";
        }
        defaultValueField.setText(defaultValue);

        FieldSelectionStyle style = currentFieldProperty.getSelectionStyle();
        selectionStyleComboBox.getSelectionModel().select(style);

        FieldSelectionSource source = currentFieldProperty.getSelectionSource();
        selectionSourceComboBox.getSelectionModel().select(source);

        String codes = currentFieldProperty.getSelectionCodesInLine();
        if (codes == null) {
            codes = "";
        }
        selectionCodesField.setText(codes);

        String dilimiter = currentFieldProperty.getSelectionDelimiter();
        if (dilimiter == null) {
            dilimiter = "";
        }
        selectionDelimiterField.setText(dilimiter);

        String positive = currentFieldProperty.getPositiveValue();
        if (positive == null) {
            positive = "";
        }
        selectionPositiveField.setText(positive);

        String negative = currentFieldProperty.getNegativeValue();
        if (negative == null) {
            negative = "";
        }
        selectionNegativeField.setText(negative);

        String attr = currentFieldProperty.getAttribute();
        if (attr == null) {
            attr = "";
        }
        selectionAttributeField.setText(attr);

        Result result = currentFieldProperty.getSelectionResult();
        selectionResultComboBox.getSelectionModel().select(result);

        String tableKey = currentFieldProperty.getSelectionTableKeys();
        selectionTableKeyField.setText(tableKey);
    }

    private void saveFieldValues() {
        if (currentFieldProperty != null) {
            currentFieldProperty.setDefaultValue(defaultValueField.getText());
            currentFieldProperty.setSelectionStyle(selectionStyleComboBox.getValue());
            currentFieldProperty.setSelectionSource(selectionSourceComboBox.getValue());
            currentFieldProperty.setSelectionCodes(selectionCodesField.getText());
            currentFieldProperty.setSelectionDelimiter(defaultValueField.getText());
            currentFieldProperty.setPositiveValue(selectionPositiveField.getText());
            currentFieldProperty.setNegativeValue(selectionNegativeField.getText());
            currentFieldProperty.setAttribute(selectionAttributeField.getText());
            currentFieldProperty.setSelectionResult(selectionResultComboBox.getValue());
            currentFieldProperty.setSelectionTableKeys(selectionTableKeyField.getText());
        }
    }

    private void load() {
        encodingField.setText(loadStringProperty(AppConfig.PROPNAME_ENCODING));
        userAgentField.setText(loadStringProperty(AppConfig.PROPNAME_USER_AGENT));
        timeoutField.setText(loadStringProperty(AppConfig.PROPNAME_CONNECTION_TIMEOUT));
        pagerSelectorField.setText(loadStringProperty(AppConfig.PROPNAME_PAGER_CSS_SELECTOR));
        pagerLinkSelectorField.setText(loadStringProperty(AppConfig.PROPNAME_PAGER_LINK_CSS_SELECTOR));
        productLinkSelectorField.setText(loadStringProperty(AppConfig.PROPNAME_PRODUCT_ANCHOR_CSS_SELECTOR));
    }

    private String loadStringProperty(String propname) {
        return AppConfig.getInstance().getProperty(propname);
    }

    private void save() {
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_ENCODING, encodingField.getText());
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_USER_AGENT, userAgentField.getText());
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_CONNECTION_TIMEOUT, timeoutField.getText());
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_PAGER_CSS_SELECTOR, pagerSelectorField.getText());
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_PAGER_LINK_CSS_SELECTOR, pagerLinkSelectorField.getText());
        AppConfig.getInstance().setProperty(AppConfig.PROPNAME_PRODUCT_ANCHOR_CSS_SELECTOR, productLinkSelectorField.getText());
        saveFieldValues();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
