package com.hereisalexius.ppp;

import com.hereisalexius.ppp.model.GroupWebPage;
import com.hereisalexius.ppp.parser.ProductInfoParser;
import com.hereisalexius.ppp.model.ProductWebPage;
import com.hereisalexius.ppp.model.WebPage;
import com.hereisalexius.ppp.utils.AppConfig;
import com.opencsv.CSVWriter;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ParserApp extends Application {

    private ProductInfoParser parser;
    ExecutorService threadPool;

    private BorderPane rootPane;
    private Scene scene;

    private HBox dowloadInputPane;
    private TextField groupUrlField;
    private Button downloadButton;
    private Button testButton;

    private VBox progressPane;
    private ProgressBar progressBar;
    private Label msgLabel;

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("prom.ua platform parser 2");
        primaryStage.setResizable(false);
        initProperties();
        rootPane = new BorderPane();
        initComponents();
        scene = new Scene(rootPane, 640, 80);
        primaryStage.setScene(scene);
        primaryStage.setOnHiding((e) -> {
            Platform.runLater(() -> {
                threadPool.shutdown();
            });
        });

        primaryStage.show();
    }

    private void initProperties() {
        AppConfig.getInstance().preLoad();
        parser = new ProductInfoParser();
        threadPool = Executors.newSingleThreadExecutor();
    }

    private void initComponents() {
        dowloadInputPane = new HBox(4);
        dowloadInputPane.setPadding(new Insets(4, 4, 4, 4));

        groupUrlField = new TextField();
        groupUrlField.setPromptText("GROUP URL");
        HBox.setHgrow(groupUrlField, Priority.ALWAYS);
        downloadButton = new Button("Parse");
        downloadButton.setOnAction((e) -> {
            parseAction(false);
        });
        testButton = new Button("Test");
        testButton.setOnAction((e) -> {
            parseAction(true);
        });
        dowloadInputPane.getChildren().addAll(groupUrlField, downloadButton, testButton);
        rootPane.setTop(dowloadInputPane);

        progressPane = new VBox(4);
        progressPane.setPadding(new Insets(4, 4, 4, 4));
        progressBar = new ProgressBar();
        progressBar.setMaxSize(9999d, 9999d);
        HBox.setHgrow(progressBar, Priority.ALWAYS);
        msgLabel = new Label("Ready for action!");

        progressPane.getChildren().addAll(new HBox(progressBar), msgLabel);
        rootPane.setCenter(progressPane);
    }

    private void parseAction(boolean isTest) {

        AppConfig.getInstance().load();
        String url = groupUrlField.getText();
        if (url != null && !url.isEmpty()) {
            downloadButton.setDisable(true);
            testButton.setDisable(true);

            Task<GroupWebPage> startPageTask = new LoadGroupWebPageTask(url);

            startPageTask.setOnSucceeded((e) -> {
                GroupWebPage gwp = startPageTask.getValue();
                loadProductsUrls(gwp, isTest);

            });

            threadPool.execute(startPageTask);

        }

    }

    private void loadProductsUrls(GroupWebPage gwp, boolean isTest) {
        Task<HashSet<String>> pagesCollectionTask = gwp.getLoadingTask();
        progressBar.progressProperty().unbind();
        progressBar.progressProperty().bind(pagesCollectionTask.progressProperty());

        msgLabel.textProperty().unbind();
        msgLabel.textProperty().bind(pagesCollectionTask.messageProperty());

        pagesCollectionTask.setOnSucceeded((e) -> {
            HashSet<String> p_urls = pagesCollectionTask.getValue();
            if (isTest && p_urls.iterator().hasNext()) {

                Task<ProductWebPage> loadTestPageTask = new LoadProductWebPageTask(p_urls.iterator().next());

                progressBar.progressProperty().unbind();
                progressBar.progressProperty().bind(loadTestPageTask.progressProperty());

                msgLabel.textProperty().unbind();
                msgLabel.textProperty().bind(loadTestPageTask.messageProperty());

                loadTestPageTask.setOnSucceeded((evt2) -> {
                    showTestResultsDialog(loadTestPageTask.getValue());
                });
                threadPool.execute(loadTestPageTask);
            } else if (p_urls.iterator().hasNext()) {
                Task<List<ProductWebPage>> downloadingTask = new LoadProductWebPagesTask(p_urls);

                progressBar.progressProperty().unbind();
                progressBar.progressProperty().bind(downloadingTask.progressProperty());

                msgLabel.textProperty().unbind();
                msgLabel.textProperty().bind(downloadingTask.messageProperty());

                downloadingTask.setOnSucceeded((evt2) -> {
                    saveToCSVFileAction(downloadingTask.getValue());
                });
                threadPool.execute(downloadingTask);

            } else {
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Warning Dialog");
                alert.setHeaderText("No Pages Loaded!");
                alert.setContentText("Please check pages selection properties.");
            }
        });

        threadPool.execute(pagesCollectionTask);
    }

    private void saveToCSVFileAction(List<ProductWebPage> pages) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Create or select CSV file");
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Comma Separated Values", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);

        File saveFile = fileChooser.showSaveDialog(null);
        if (saveFile != null) {
            saveToCSVFile(saveFile, pages);
        }
    }

    private void saveToCSVFile(File saveFile, List<ProductWebPage> pages) {
        Task<Void> saveTask = new SavingDataTask(saveFile, pages);

        progressBar.progressProperty().unbind();
        progressBar.progressProperty().bind(saveTask.progressProperty());

        msgLabel.textProperty().unbind();
        msgLabel.textProperty().bind(saveTask.messageProperty());

        saveTask.setOnSucceeded((evt2) -> {
            downloadButton.setDisable(false);
            testButton.setDisable(false);
        });
        threadPool.execute(saveTask);
    }

    private void showTestResultsDialog(ProductWebPage pwp) {
        downloadButton.setDisable(false);
        testButton.setDisable(false);

        Alert testInfoAlert = new Alert(Alert.AlertType.INFORMATION);
        testInfoAlert.setTitle("Test Dialog");
        testInfoAlert.setHeaderText("Test Parsing Result");
        testInfoAlert.setContentText("For better result, please make sure that all selection properties were set.");

        Label label = new Label("[" + pwp.getUrl() + "]\nCSV: ");

        TextArea textArea = new TextArea(pwp.asCSV(";"));
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane expContent = new GridPane();
        expContent.setMaxWidth(Double.MAX_VALUE);
        expContent.add(label, 0, 0);
        expContent.add(textArea, 0, 1);

        testInfoAlert.getDialogPane().setExpandableContent(expContent);

        testInfoAlert.showAndWait();

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

    private class LoadGroupWebPageTask extends Task<GroupWebPage> {

        private final String url;

        public LoadGroupWebPageTask(String url) {
            this.url = url;
        }

        @Override
        protected GroupWebPage call() throws Exception {

            return new GroupWebPage(url);
        }
    }

    private class LoadProductWebPageTask extends Task<ProductWebPage> {

        private final String url;

        public LoadProductWebPageTask(String url) {
            this.url = url;
        }

        @Override
        protected ProductWebPage call() throws Exception {
            return new ProductWebPage(url, parser);
        }
    }

    private class LoadProductWebPagesTask extends Task<List<ProductWebPage>> {

        private final HashSet<String> urls;

        public LoadProductWebPagesTask(HashSet<String> urls) {
            this.urls = urls;
        }

        @Override
        protected List<ProductWebPage> call() throws Exception {
            updateMessage("Starting to download product pages...");
            updateProgress(0, 1);
            List<ProductWebPage> result = new ArrayList<>();
            for (String url : urls) {
                result.add(new ProductWebPage(url, parser));
                updateMessage("downloaded : " + url);
                updateProgress(result.size(), urls.size());
            }
            updateMessage("All product pages downloaded.");
            updateProgress(1, 1);
            return result;

        }
    }

    private class SavingDataTask extends Task<Void> {

        private final File saveFile;
        private final List<ProductWebPage> pages;

        public SavingDataTask(File saveFile, List<ProductWebPage> pages) {
            this.saveFile = saveFile;
            this.pages = pages;
        }

        @Override
        protected Void call() throws Exception {
            updateMessage("Starting to parse and write...");
            updateProgress(0, 1);
            CSVWriter writer = null;
            try {
                writer = new CSVWriter(new FileWriter(saveFile, true));
                int counter = 1;
                for (ProductWebPage page : pages) {
                    writer.writeNext(page.asArray());
                    updateMessage("parsed and saved : " + page.getUrl());
                    updateProgress(counter++, pages.size());
                }
            } catch (IOException e) {
                if (writer != null) {
                    writer.close();
                }
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    writer.close();
                }
            }

            updateMessage("All is done.");
            updateProgress(1, 1);

            return null;
        }

    }

}
