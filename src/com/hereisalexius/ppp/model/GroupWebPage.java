package com.hereisalexius.ppp.model;

import com.hereisalexius.ppp.utils.AppConfig;
import java.io.File;
import java.util.*;
import javafx.concurrent.Task;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class GroupWebPage extends WebPage {

    public GroupWebPage(String url) {
        super(url);
    }

    @Override
    protected String fileNameGenerator(String url) {
        System.out.println(url);
        String filename = url.replaceAll("/|:", "_");
        System.out.println(filename);
        return filename;
    }

    public Task<HashSet<String>> getLoadingTask() {
        return new Task<HashSet<String>>() {

            @Override
            protected HashSet<String> call() throws Exception {
                updateMessage("Starting to collect products pages urls...");
                return getProductURLs();
            }

            private HashSet<String> getProductURLs() {

                HashSet<String> productsURLs = new HashSet<>();
                updateMessage("(2) Starting to collect product pages...");
                updateProgress(0, 1);
                List<String> purls = getPagesURLs();
                for (String pagUrl : purls) {
                    GroupWebPage page = new GroupWebPage(pagUrl);
                    Elements productAnchors = page.toJsoupDocument().select(AppConfig.getInstance().getProperty(AppConfig.PROPNAME_PRODUCT_ANCHOR_CSS_SELECTOR));
                    for (Element pa : productAnchors) {
                        String url = pa.absUrl("href");
                        updateMessage("(2) found : " + url);
                        updateProgress(productsURLs.size(), productAnchors.size() * purls.size());
                        productsURLs.add(url);
                    }
                }

                updateMessage("(2) Product pages collected...");
                updateProgress(1, 1);

                return productsURLs;
            }

            private List<String> getPagesURLs() {
                updateMessage("(1) Starting to collect group pages...");
                updateProgress(0, 1);
                List<String> urls = new ArrayList<String>();

                Document firstPage = toJsoupDocument();

                Elements pager = firstPage.select(AppConfig.getInstance().getProperty(AppConfig.PROPNAME_PAGER_CSS_SELECTOR));
                if (pager.isEmpty()) {
                    urls.add(url);
                    updateMessage("(1) found : " + url);
                } else {
                    Elements pages = pager.select(AppConfig.getInstance().getProperty(AppConfig.PROPNAME_PAGER_LINK_CSS_SELECTOR));
                    int pageCount = Integer.valueOf(pages.get(pages.size() - 2).text());
                    for (int i = 1; i <= pageCount; i++) {
                        String subUrl = url + "/page_" + i;
                        urls.add(subUrl);
                        updateMessage("(1) found : " + subUrl);
                        updateProgress(i, pageCount);
                    }
                }
                updateMessage("(1) Group pages collected.");
                updateProgress(1, 1);
                return urls;
            }
        };
    }

}
