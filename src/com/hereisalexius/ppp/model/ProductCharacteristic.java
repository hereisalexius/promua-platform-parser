package com.hereisalexius.ppp.model;



public class ProductCharacteristic {
    private String name;
    private String measure;
    private String value;

    public ProductCharacteristic(String name, String value) {
        this.name = name;
        this.measure = "";
        this.value = value;
    }
    
    public ProductCharacteristic(String name, String measure, String value) {
        this.name = name;
        this.measure = measure;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
