package com.hereisalexius.ppp.model;

import java.util.List;

public interface ProductInfo {

    public String getCode();

    public String getPositionTitle();

    public String getKeyWords();

    public String getDescription();

    public String getType();
    
    public String getPrice();
    
    public String getPriceCurrency();

    public String getMeasure();

    public String getMinOrderCount();

    public String getWholesalePrice();

    public String getMinWholesaleCount();

    public String getImageURLs();

    public String getState();

    public String getCount();

    public String getGroupNumber();

    public String getSubsectionAddress();

    public String getDeliveryPosibility();

    public String getDeliveryTerm();

    public String getPackingType();

    public String getUniqID();

    public String getID();

    public String getSubsectionID();

    public String getGroupID();

    public String getProducer();

    public String getGuarantyTerm();

    public String getProducerCountry();

    public String getDiscount();

    public String getDifferenceGroupID();

    public String getProducerTitle();

    public String getProducerAddress();

    public String getTotalCharacteristics();

    public String getUserTotalCharacteristics();

    public List<ProductCharacteristic> getCharacteristics();

}
