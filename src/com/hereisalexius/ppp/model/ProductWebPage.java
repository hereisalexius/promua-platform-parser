package com.hereisalexius.ppp.model;

import com.hereisalexius.ppp.parser.ProductInfoParser;
import java.io.IOException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;

public class ProductWebPage extends WebPage implements ProductInfo, StringArrayConvertable {

    private ProductInfoParser parser;

    public ProductWebPage(String url, ProductInfoParser parser) {
        super(url);
        this.parser = parser;
    }

    @Override
    public String getCode() {
        return parser.getCodeParser().parse(toJsoupDocument());
    }

    @Override
    public String getPositionTitle() {
        return parser.getPositionTitleParser().parse(toJsoupDocument());
    }

    @Override
    public String getKeyWords() {
        return parser.getKeyWordsParser().parse(toJsoupDocument());
    }

    @Override
    public String getDescription() {
        return parser.getDescriptionParser().parse(toJsoupDocument());
    }

    @Override
    public String getType() {
        return parser.getTypeParser().parse(toJsoupDocument());
    }

    @Override
    public String getPrice() {
        return parser.getPriceParser().parse(toJsoupDocument());
    }

    @Override
    public String getPriceCurrency() {
        return parser.getPriceCurrencyParser().parse(toJsoupDocument());
    }

    @Override
    public String getMeasure() {
        return parser.getMeasureParser().parse(toJsoupDocument());
    }

    @Override
    public String getMinOrderCount() {
        return parser.getMinOrderCountParser().parse(toJsoupDocument());
    }

    @Override
    public String getWholesalePrice() {
        return parser.getWholesalePriceParser().parse(toJsoupDocument());
    }

    @Override
    public String getMinWholesaleCount() {
        return parser.getMinWholesaleCountParser().parse(toJsoupDocument());
    }

    @Override
    public String getImageURLs() {
        return parser.getImageURLsParser().parse(toJsoupDocument());
    }

    @Override
    public String getState() {
        return parser.getStateParser().parse(toJsoupDocument());
    }

    @Override
    public String getCount() {
        return parser.getCountParser().parse(toJsoupDocument());
    }

    @Override
    public String getGroupNumber() {
        return parser.getGroupNumberParser().parse(toJsoupDocument());
    }

    @Override
    public String getSubsectionAddress() {
        return parser.getSubsectionAddressParser().parse(toJsoupDocument());
    }

    @Override
    public String getDeliveryPosibility() {
        return parser.getDeliveryPosibilityParser().parse(toJsoupDocument());
    }

    @Override
    public String getDeliveryTerm() {
        return parser.getDeliveryTermParser().parse(toJsoupDocument());
    }

    @Override
    public String getPackingType() {
        return parser.getPackingTypeParser().parse(toJsoupDocument());
    }

    @Override
    public String getUniqID() {
        return parser.getUniqIDParser().parse(toJsoupDocument());
    }

    @Override
    public String getID() {
        return parser.getIDParser().parse(toJsoupDocument());
    }

    @Override
    public String getSubsectionID() {
        return parser.getSubsectionIDParser().parse(toJsoupDocument());
    }

    @Override
    public String getGroupID() {
        return parser.getGroupIDParser().parse(toJsoupDocument());
    }

    @Override
    public String getProducer() {
        return parser.getProducerParser().parse(toJsoupDocument());
    }

    @Override
    public String getGuarantyTerm() {
        return parser.getGuarantyTermParser().parse(toJsoupDocument());
    }

    @Override
    public String getProducerCountry() {
        return parser.getProducerCountryParser().parse(toJsoupDocument());
    }

    @Override
    public String getDiscount() {
        return parser.getDiscountParser().parse(toJsoupDocument());
    }

    @Override
    public String getDifferenceGroupID() {
        return parser.getDifferenceGroupIDParser().parse(toJsoupDocument());
    }

    @Override
    public String getProducerTitle() {
        return parser.getProducerTitleParser().parse(toJsoupDocument());
    }

    @Override
    public String getProducerAddress() {
        return parser.getProducerAddressParser().parse(toJsoupDocument());
    }

    @Override
    public String getTotalCharacteristics() {
        return parser.getTotalCharacteristicsParser().parse(toJsoupDocument());
    }

    @Override
    public String getUserTotalCharacteristics() {
        return parser.getUserTotalCharacteristicsParser().parse(toJsoupDocument());
    }

    @Override
    public List<ProductCharacteristic> getCharacteristics() {
        List<ProductCharacteristic> characteristics = new ArrayList<>();
        String raw = parser.getCharacteristicsParser().parse(toJsoupDocument());
        String[] rawSets = raw.split(";");
        for (String rawset : rawSets) {
            characteristics.add(new ProductCharacteristic(
                    rawset.split(",")[0],
                    rawset.split(",")[1])
            );
        }

        return characteristics;
    }

    public List<String> getCharacteristicsAsList() {
        List<String> row = new ArrayList<>();
        for (ProductCharacteristic c : getCharacteristics()) {
            row.add(c.getName());
            row.add(c.getMeasure());
            row.add(c.getValue());
        }
        return row;
    }

    @Override
    public String[] asArray() {
        List<String> row = new ArrayList<>();
        row.add(getCode());
        row.add(getPositionTitle());
        row.add(getKeyWords());
        row.add(getDescription());
        row.add(getType());
        row.add(getPrice());
        row.add(getPriceCurrency());
        row.add(getMeasure());
        row.add(getMinOrderCount());
        row.add(getWholesalePrice());
        row.add(getMinWholesaleCount());
        row.add(getImageURLs());
        row.add(getState());
        row.add(getCount());
        row.add(getGroupNumber());
        row.add(getSubsectionAddress());
        row.add(getDeliveryPosibility());
        row.add(getDeliveryTerm());
        row.add(getPackingType());
        row.add(getUniqID());
        row.add(getID());
        row.add(getSubsectionID());
        row.add(getGroupID());
        row.add(getProducer());
        row.add(getGuarantyTerm());
        row.add(getProducerCountry());
        row.add(getDiscount());
        row.add(getDifferenceGroupID());
        row.add(getProducerTitle());
        row.add(getProducerAddress());
        row.add(getTotalCharacteristics());
        row.add(getUserTotalCharacteristics());
        row.addAll(getCharacteristicsAsList());
        return row.toArray(new String[row.size()]);
    }

    @Override
    public String asCSV(String del) {
        String result = "";
        for (String val : asArray()) {
            result += ";" + val;
        }
        return result.substring(1);
    }

}
