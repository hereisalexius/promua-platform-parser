/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hereisalexius.ppp.model;

/**
 *
 * @author Olexiy
 */
public interface StringArrayConvertable {

    public String[] asArray();

    public String asCSV(String del);
}
