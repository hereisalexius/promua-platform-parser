package com.hereisalexius.ppp.model;

import com.hereisalexius.ppp.utils.AppConfig;
import com.hereisalexius.ppp.utils.SimpleConnection;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class WebPage {
    
    protected String url;
    protected File htmlPageFile;
    
    public WebPage(String url) {
        this.url = url;
        this.htmlPageFile = downloadPage(url);
    }
    
    protected File downloadPage(String url) {
        File directory = loadDomainFolder(url);
        String fileName = fileNameGenerator(url);
        File webpage = new File(directory, fileName);
        if (!webpage.exists()) {
            writeHtmlToFile(webpage);
        }
        return webpage;
    }
    
    protected String fileNameGenerator(String url) {
        String[] splitedUrl = url.split("/");
        String fileName = splitedUrl[splitedUrl.length - 1];
        return fileName;
    }
    
    private void writeHtmlToFile(File webpage) {
        try {
            Document doc = SimpleConnection.connect(url);
            FileWriter fw = new FileWriter(webpage);
            fw.append(doc.html());
            fw.flush();
            fw.close();
            
        } catch (IOException ex) {
            Logger.getLogger(WebPage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private File loadPagesFolder() {
        File pagesFolder = new File(System.getProperty("user.home"), "parser_pages");
        if (!pagesFolder.exists()) {
            pagesFolder.mkdir();
        }
        return pagesFolder;
    }
    
    private File loadDomainFolder(String url) {
        loadPagesFolder();
        String folderName = url
                .replaceAll("http.*//", "")
                .replaceAll("/.*", "")
                .replaceAll("\\.", "_");
        
        File domainFolder = new File(System.getProperty("user.home"), "parser_pages/" + folderName);
        if (!domainFolder.exists()) {
            domainFolder.mkdir();
        }
        
        return domainFolder;
    }
    
    public String getUrl() {
        return url;
    }
    
    public Document toJsoupDocument() {
        Document doc = null;
        try {
            doc = Jsoup.parse(htmlPageFile, AppConfig.getInstance().getProperty(AppConfig.PROPNAME_ENCODING));
        } catch (IOException ex) {
            Logger.getLogger(WebPage.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            return doc;
        }
    }
    
    public File getHtmlPageFile() {
        return htmlPageFile;
    }
}
