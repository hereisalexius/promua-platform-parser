package com.hereisalexius.ppp.parser;

import org.jsoup.nodes.Document;

public interface Parseable {
    public String parse(Document document);
}
