package com.hereisalexius.ppp.parser;

import com.hereisalexius.ppp.utils.AppConfig;
import com.hereisalexius.ppp.utils.FieldProperty;
import com.hereisalexius.ppp.utils.FieldSelectionSource;
import com.hereisalexius.ppp.utils.ProductField;
import com.hereisalexius.ppp.utils.Result;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParserBuilder {

    public static Parseable build(ProductField field) {
        FieldProperty prop = new FieldProperty(field);
        try {
            switch (prop.getSelectionStyle()) {
                case SIMPLE_CSS_SELECTOR:
                    return simpleCssSelection(prop);
                case MULTIPLY_CSS_SELECTOR:
                    return multiplyCssSelection(prop);
                case CONTAINS_TEXT:
                    return new ContainsTextParser(
                            prop.getSelectionCodesInLine(),
                            prop.getPositiveValue(),
                            prop.getNegativeValue(),
                            prop.getSelectionSource() == FieldSelectionSource.HTML);
                case BINARY_TABLE_CSS_SELECTOR:
                    return new BinaryTableCssSelectorParser(
                            prop.getDefaultValue(),
                            prop.getSelectionCodesInLine());
                case VALUE_FROM_BINARY_TABLE_CSS_SELECTOR:
                    return new ValueFromBinaryTableCssSelectorParser(
                            prop.getDefaultValue(),
                            prop.getSelectionCodesInLine(),
                            prop.getSelectionTableKeys()
                    );
                default:
                    return new EmptyParser(prop.getDefaultValue());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            AppConfig.showErrorAlert("Error Dialog",
                    "Parsing Error",
                    "Please check selection properties validation (for field " + field + ")",
                     ex);
            return new EmptyParser("ERROR");
        }
    }

    private static Parseable simpleCssSelection(FieldProperty prop) {
        switch (prop.getSelectionSource()) {
            case HTML:
                return new SimpleCssSelectorParser(
                        prop.getDefaultValue(),
                        prop.getSelectionCodesInLine(),
                        prop.getSelectionResult() == Result.HTML);
            case ATTRIBUTE:
                return new SimpleCssSelectorFromAttrParser(
                        prop.getAttribute(),
                        prop.getDefaultValue(),
                        prop.getSelectionCodesInLine()
                );
            default:
                return new EmptyParser(prop.getDefaultValue());
        }
    }

    private static Parseable multiplyCssSelection(FieldProperty prop) {
        switch (prop.getSelectionSource()) {
            case HTML:
                return new SimpleCssSelectorParser(
                        prop.getDefaultValue(),
                        prop.getSelectionCodesInLine(),
                        prop.getSelectionResult() == Result.HTML);
            case ATTRIBUTE:
                return new MultiplyCssSelectorFromAttrParser(
                        prop.getAttribute(),
                        prop.getDefaultValue(),
                        prop.getSelectionCodesInLine(),
                        prop.getSelectionDelimiter());
            default:
                return new EmptyParser(prop.getDefaultValue());
        }
    }

    private static class EmptyParser implements Parseable {

        private final String defaultValue;

        public EmptyParser(String defaultValue) {
            this.defaultValue = defaultValue;
        }

        @Override
        public String parse(Document document) {
            return defaultValue;
        }
    }

    private static class SimpleCssSelectorParser implements Parseable {

        protected final String defaultValue;
        protected final String cssSeletor;
        protected final boolean returnHtml;

        public SimpleCssSelectorParser(String defaultValue, String cssSeletor, boolean returnHtml) {
            this.defaultValue = defaultValue;
            this.cssSeletor = cssSeletor;
            this.returnHtml = returnHtml;
        }

        @Override
        public String parse(Document document) {
            String result = document.select(cssSeletor).text();
            if (returnHtml) {
                result = document.select(cssSeletor).html().replaceAll("&nbsp;", "");
            }
            if (result != null && !result.isEmpty()) {
                return result.replaceAll("\\n+", " ").trim();
            }
            return defaultValue;
        }
    }

    private static class SimpleCssSelectorFromAttrParser extends SimpleCssSelectorParser {

        protected final String attr;

        public SimpleCssSelectorFromAttrParser(String attr, String defaultValue, String cssSeletor) {
            super(defaultValue, cssSeletor, false);
            this.attr = attr;
        }

        @Override
        public String parse(Document document) {
            String[] attrs = attr.split(",");

            for (String att : attrs) {
                String result = document.select(cssSeletor).attr(att);
                if (result != null && !result.isEmpty()) {
                    return result;
                }
            }
            return defaultValue;
        }
    }

    private static class MultiplyCssSelectorFromAttrParser extends SimpleCssSelectorFromAttrParser {

        private final String del;

        public MultiplyCssSelectorFromAttrParser(String attr, String defaultValue, String cssSeletor, String del) {
            super(attr, defaultValue, cssSeletor);
            this.del = del;
        }

        @Override
        public String parse(Document document) {
            String[] attrs = attr.split(",");
            String result = "";
            Elements elementns = document.select(cssSeletor);

            for (String att : attrs) {
                for (Element e : elementns) {
                    if (e != null) {
                        String val = e.attr(att);
                        if (!val.isEmpty()) {
                            result += del + val;
                        }
                    }
                }
            }

            if (result != null && !result.isEmpty()) {
                if (result.startsWith(del)) {
                    result = result.substring(del.length());
                }
                return result;
            }
            return defaultValue;
        }
    }

    private static class ContainsTextParser implements Parseable {

        private final String text;
        private final String positive;
        private final String negative;

        private final boolean isInHtml;

        public ContainsTextParser(String text, String positive, String negative, boolean isInHtml) {
            this.text = text;
            this.positive = positive;
            this.negative = negative;
            this.isInHtml = isInHtml;
        }

        @Override
        public String parse(Document document) {
            String wholeText = document.text();
            if (isInHtml) {
                wholeText = document.html();
            }
            if (wholeText.contains(text)) {
                return positive;
            }
            return negative;
        }
    }

    public static class BinaryTableCssSelectorParser implements Parseable {

        protected final String defaultValue;
        protected final String cssSelector;

        public BinaryTableCssSelectorParser(String defaultValue, String cssSelector) {
            this.defaultValue = defaultValue;
            this.cssSelector = cssSelector;
        }

        protected Map<String, String> getTable(Document productPage) {
            Map<String, String> result = new HashMap<String, String>();

            Element table = productPage.select(cssSelector).first();

            if (table != null) {
                Elements trs = table.select("tr");

                for (Element tr : trs) {
                    if (tr.select("td").first() != null) {
                        String key = tr.select("td").get(0).text();
                        if (key.contains("Производитель")) {
                            key = "Производитель";
                        }
                        result.put(key.trim(), tr.select("td").get(1).text().trim());

                    }
                }
            }
            return result;

        }

        @Override
        public String parse(Document document) {
            String result = "";
            Map<String, String> table = getTable(document);
            if (!table.isEmpty()) {
                for (Map.Entry<String, String> row : table.entrySet()) {
                    result += ";" + row.getKey() + "," + row.getValue();
                }
                return result.substring(1);
            }
            return defaultValue;
        }
    }

    public static class ValueFromBinaryTableCssSelectorParser extends BinaryTableCssSelectorParser {

        private final String keys;

        public ValueFromBinaryTableCssSelectorParser(String defaultValue, String cssSelector, String keys) {
            super(defaultValue, cssSelector);
            this.keys = keys;
        }

//        public ValueFromBinaryTableCssSelectorParser(String defaultValue, String cssSelector, String keys) {
//            this.defaultValue = defaultValue;
//            this.cssSelector = cssSelector;
//            this.keys = keys;
//        }
        @Override
        public String parse(Document document) {

            Map<String, String> table = getTable(document);
            if (!table.isEmpty()) {
                for (String key : keys.split(",")) {
                    for (Map.Entry<String, String> row : table.entrySet()) {
                        if (row.getKey().toLowerCase().trim().contentEquals(key.toLowerCase().trim())) {
                            return row.getValue();
                        }
                    }

                }
            }
            return defaultValue;
        }
    }

}
