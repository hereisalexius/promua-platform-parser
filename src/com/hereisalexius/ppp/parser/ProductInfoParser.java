package com.hereisalexius.ppp.parser;

import com.hereisalexius.ppp.parser.Parseable;
import com.hereisalexius.ppp.utils.FieldProperty;
import com.hereisalexius.ppp.utils.ProductField;
import java.util.List;
import org.jsoup.nodes.Document;

public class ProductInfoParser {

    public Parseable getCodeParser() {
        return ParserBuilder.build(ProductField.CODE);
    }

    public Parseable getPositionTitleParser() {
        return ParserBuilder.build(ProductField.POSITION_TITLE);
    }

    public Parseable getKeyWordsParser() {
        return ParserBuilder.build(ProductField.KEYWORDS);
    }

    public Parseable getDescriptionParser() {
        return ParserBuilder.build(ProductField.DESCRIPTION);
    }

    public Parseable getTypeParser() {
        return ParserBuilder.build(ProductField.TYPE);
    }

    public Parseable getPriceParser() {
        return ParserBuilder.build(ProductField.PRICE);
    }

    public Parseable getPriceCurrencyParser() {
        return ParserBuilder.build(ProductField.CURRENCY);
    }

    public Parseable getMeasureParser() {
        return ParserBuilder.build(ProductField.MEASURE);
    }

    public Parseable getMinOrderCountParser() {
        return ParserBuilder.build(ProductField.MIN_ORDER_COUNT);
    }

    public Parseable getWholesalePriceParser() {
        return ParserBuilder.build(ProductField.WHOLESALE_PRICE);
    }

    public Parseable getMinWholesaleCountParser() {
        return ParserBuilder.build(ProductField.MIN_WHOLESALE_COUNT);
    }

    public Parseable getImageURLsParser() {
        return ParserBuilder.build(ProductField.IMAGE_URLS);
    }

    public Parseable getStateParser() {
        return ParserBuilder.build(ProductField.STATE);
    }

    public Parseable getCountParser() {
        return ParserBuilder.build(ProductField.COUNT);
    }

    public Parseable getGroupNumberParser() {
        return ParserBuilder.build(ProductField.GROUP_NUMBER);
    }

    public Parseable getSubsectionAddressParser() {
        return ParserBuilder.build(ProductField.SUBSECTION_ADDRESS);
    }

    public Parseable getDeliveryPosibilityParser() {
        return ParserBuilder.build(ProductField.DELIVERY_POSIBILITY);
    }

    public Parseable getDeliveryTermParser() {
        return ParserBuilder.build(ProductField.DELIVERY_TERM);
    }

    public Parseable getPackingTypeParser() {
        return ParserBuilder.build(ProductField.PACKING_TYPE);
    }

    public Parseable getUniqIDParser() {
        return ParserBuilder.build(ProductField.UNIQ_ID);
    }

    public Parseable getIDParser() {
        return ParserBuilder.build(ProductField.ID);
    }

    public Parseable getSubsectionIDParser() {
        return ParserBuilder.build(ProductField.SUBSECTON_ID);
    }

    public Parseable getGroupIDParser() {
        return ParserBuilder.build(ProductField.GROUP_ID);
    }

    public Parseable getProducerParser() {
        return ParserBuilder.build(ProductField.PRODUCER);
    }

    public Parseable getGuarantyTermParser() {
        return ParserBuilder.build(ProductField.GUARANTY_TERM);
    }

    public Parseable getProducerCountryParser() {
        return ParserBuilder.build(ProductField.PRODUCER_COUNTRY);
    }

    public Parseable getDiscountParser() {
        return ParserBuilder.build(ProductField.DISCOUNT);
    }

    public Parseable getDifferenceGroupIDParser() {
        return ParserBuilder.build(ProductField.DIFFERENCE_GROUP_ID);
    }

    public Parseable getProducerTitleParser() {
        return ParserBuilder.build(ProductField.PRODUCER_TITLE);
    }

    public Parseable getProducerAddressParser() {
        return ParserBuilder.build(ProductField.PRODUCER_ADDRESS);
    }

    public Parseable getTotalCharacteristicsParser() {
        return ParserBuilder.build(ProductField.TOTAL_CHARACTERISTICS);
    }

    public Parseable getUserTotalCharacteristicsParser() {
        return ParserBuilder.build(ProductField.USER_TOTAL_CHARACTERISTICS);
    }

    public Parseable getCharacteristicsParser() {
        return ParserBuilder.build(ProductField.CHARACTERISTICS);
    }

}
