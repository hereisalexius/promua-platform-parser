package com.hereisalexius.ppp.utils;

import java.io.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class AppConfig {

    //todo selection styles enum
    public static final String APP_NAME = "PromUA Platform Parser 2[03.2018]";

    public static final String PROPNAME_USER_AGENT = "userAgent";
    public static final String PROPNAME_CONNECTION_TIMEOUT = "connectionTimeout";
    public static final String PROPNAME_PRODUCT_ANCHOR_CSS_SELECTOR = "productAnchorCssSelector";
    public static final String PROPNAME_PAGER_CSS_SELECTOR = "pagerCssSelector";
    public static final String PROPNAME_PAGER_LINK_CSS_SELECTOR = "pagerLinkCssSelector";
    public static final String PROPNAME_ENCODING = "encoding";

    private static final Logger logger = Logger.getLogger(AppConfig.class.getName());
    private Properties properties;
    private File configfile;

    private static AppConfig configInstance = new AppConfig();

    public static AppConfig getInstance() {
        return configInstance;
    }

    private AppConfig() {
        configfile = new File("parser.properties");
        init(configfile);
    }

    public void preLoad() {
        if (!configfile.exists()) {
            try {
                loadDefault();
                save();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            load();
        }
    }

    private void init(File configfile) {
        this.configfile = configfile;
        properties = new Properties();
        logger.log(Level.INFO, "Reading configuration file: {0}", configfile.getAbsolutePath());
    }

    public void setProperty(String sKey, String sValue) {
        if (sValue == null) {
            properties.remove(sKey);
        } else {
            properties.setProperty(sKey, sValue);
        }
    }

    public boolean delete() {
        loadDefault();
        return configfile.delete();
    }

    public void load() {

        try {
            InputStream in = new FileInputStream(configfile);
            if (in != null) {
                properties.load(in);
                in.close();
            }
        } catch (IOException e) {
            loadDefault();
        }
    }

    public void save() throws IOException {
        OutputStream out = new FileOutputStream(configfile);
        if (out != null) {
            properties.store(out, APP_NAME + ". Configuration file.");
            out.close();
        }
    }

    private void loadDefault() {

        properties = new Properties();

        String dirname = System.getProperty("dirname.path");
        dirname = dirname == null ? "./" : dirname;

        FieldProperty codeProp = new FieldProperty(ProductField.CODE);
        codeProp.setDefaultValue("");
        codeProp.setSelectionStyle(FieldSelectionStyle.NONE);
        codeProp.setSelectionSource(FieldSelectionSource.NONE);
        codeProp.setSelectionDelimiter("null");
        codeProp.setSelectionCodes("null");
        codeProp.setPositiveValue("");
        codeProp.setNegativeValue("");

        FieldProperty posTitleProp = new FieldProperty(ProductField.POSITION_TITLE);
        posTitleProp.setDefaultValue("Not Found!");
        posTitleProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        posTitleProp.setSelectionSource(FieldSelectionSource.HTML);
        posTitleProp.setSelectionCodes("span[data-qaid=product_name]", "h1.b-title_loc_product");
        posTitleProp.setSelectionResult(Result.TEXT);

        FieldProperty keywordProp = new FieldProperty(ProductField.KEYWORDS);
        keywordProp.setDefaultValue("Not Found!");
        keywordProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        keywordProp.setSelectionSource(FieldSelectionSource.ATTRIBUTE);
        keywordProp.setAttribute("content");
        keywordProp.setSelectionCodes("meta[name=keywords]");
        keywordProp.setSelectionResult(Result.TEXT);

        FieldProperty descProp = new FieldProperty(ProductField.DESCRIPTION);
        descProp.setDefaultValue("Not Found!");
        descProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        descProp.setSelectionSource(FieldSelectionSource.HTML);
        descProp.setSelectionCodes("div[data-qaid=product_description]");
        descProp.setSelectionResult(Result.HTML);

        initDefault(ProductField.TYPE, "r");

        FieldProperty priceProp = new FieldProperty(ProductField.PRICE);
        priceProp.setDefaultValue("Not Found!");
        priceProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        priceProp.setSelectionSource(FieldSelectionSource.HTML);
        priceProp.setSelectionCodes("span[data-qaid=product_price]");
        priceProp.setSelectionResult(Result.TEXT);

        FieldProperty currencyProp = new FieldProperty(ProductField.CURRENCY);
        currencyProp.setDefaultValue("Not Found!");
        currencyProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        currencyProp.setSelectionSource(FieldSelectionSource.HTML);
        currencyProp.setSelectionCodes("span[data-qaid=currency]");
        currencyProp.setSelectionResult(Result.TEXT);

        initDefault(ProductField.MEASURE, "шт.");
        initEmpty(ProductField.MIN_ORDER_COUNT);
        initEmpty(ProductField.WHOLESALE_PRICE);
        initEmpty(ProductField.MIN_WHOLESALE_COUNT);

        FieldProperty imageProp = new FieldProperty(ProductField.IMAGE_URLS);
        imageProp.setDefaultValue("Not Found!");
        imageProp.setSelectionStyle(FieldSelectionStyle.MULTIPLY_CSS_SELECTOR);
        imageProp.setSelectionSource(FieldSelectionSource.ATTRIBUTE);
        imageProp.setAttribute("href");
        imageProp.setSelectionDelimiter(",");
        imageProp.setSelectionCodes("a.b-product-view__image-link, a.b-extra-photos__item");
        imageProp.setSelectionResult(Result.TEXT);

        FieldProperty stateProp = new FieldProperty(ProductField.STATE);
        stateProp.setSelectionStyle(FieldSelectionStyle.CONTAINS_TEXT);
        stateProp.setSelectionSource(FieldSelectionSource.HTML);
        stateProp.setPositiveValue("+");
        stateProp.setNegativeValue("-");
        stateProp.setSelectionCodes("В наличии");

        initEmpty(ProductField.COUNT);
        initDefault(ProductField.GROUP_NUMBER, "{group_num}");
        initEmpty(ProductField.SUBSECTION_ADDRESS);
        initEmpty(ProductField.DELIVERY_POSIBILITY);
        initEmpty(ProductField.DELIVERY_TERM);
        initEmpty(ProductField.PACKING_TYPE);

        FieldProperty productIdProp = new FieldProperty(ProductField.UNIQ_ID);
        productIdProp.setDefaultValue("Not Found!");
        productIdProp.setSelectionStyle(FieldSelectionStyle.SIMPLE_CSS_SELECTOR);
        productIdProp.setSelectionSource(FieldSelectionSource.ATTRIBUTE);
        productIdProp.setAttribute("data-product-id");
        productIdProp.setSelectionCodes("[data-product-id]");
        productIdProp.setSelectionResult(Result.TEXT);

        initEmpty(ProductField.ID);
        initEmpty(ProductField.SUBSECTON_ID);
        initEmpty(ProductField.GROUP_ID);

        FieldProperty prodTab = new FieldProperty(ProductField.PRODUCER);
        prodTab.setDefaultValue("");
        prodTab.setSelectionStyle(FieldSelectionStyle.VALUE_FROM_BINARY_TABLE_CSS_SELECTOR);
        prodTab.setSelectionTableKeys("Производитель");
        prodTab.setSelectionCodes("table.b-product-info");

        initEmpty(ProductField.GUARANTY_TERM);

        FieldProperty prodCountryTab = new FieldProperty(ProductField.PRODUCER_COUNTRY);
        prodCountryTab.setDefaultValue("");
        prodCountryTab.setSelectionStyle(FieldSelectionStyle.VALUE_FROM_BINARY_TABLE_CSS_SELECTOR);
        prodCountryTab.setSelectionTableKeys("Страна производитель");
        prodCountryTab.setSelectionCodes("table.b-product-info");

        initEmpty(ProductField.DISCOUNT);
        initEmpty(ProductField.DIFFERENCE_GROUP_ID);
        initEmpty(ProductField.PRODUCER_TITLE);
        initEmpty(ProductField.PRODUCER_ADDRESS);
        initEmpty(ProductField.TOTAL_CHARACTERISTICS);
        initEmpty(ProductField.USER_TOTAL_CHARACTERISTICS);

        FieldProperty charTab = new FieldProperty(ProductField.CHARACTERISTICS);
        charTab.setDefaultValue(",");
        charTab.setSelectionStyle(FieldSelectionStyle.BINARY_TABLE_CSS_SELECTOR);
        charTab.setSelectionCodes("table.b-product-info");

        setProperty(PROPNAME_USER_AGENT,
                "Mozilla/5.0 (Windows NT 6.1) "
                + "AppleWebKit/537.36 "
                + "(KHTML, like Gecko) "
                + "Chrome/41.0.2228.0 "
                + "Safari/537.36");

        setProperty(PROPNAME_CONNECTION_TIMEOUT, "15000");
        setProperty(PROPNAME_PRODUCT_ANCHOR_CSS_SELECTOR, "a.b-product-gallery__title,a.b-product-line__product-name-link");
        setProperty(PROPNAME_PAGER_CSS_SELECTOR, "div.b-pager");
        setProperty(PROPNAME_PAGER_LINK_CSS_SELECTOR, "a.b-pager__link");
        setProperty(PROPNAME_ENCODING, "Windows-1251");

    }

    private void initDefault(ProductField pf, String text) {
        FieldProperty prop = new FieldProperty(pf);
        prop.setDefaultValue(text);
        prop.setSelectionStyle(FieldSelectionStyle.NONE);
    }

    private void initEmpty(ProductField pf) {
        initDefault(pf, "");
    }

    public String getProperty(String sKey) {
        return properties.getProperty(sKey);
    }

    public static void showErrorAlert(String title, String header, String content, Exception ex) {
        Platform.runLater(() -> {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle(title);
            alert.setHeaderText(header);
            alert.setContentText(content);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("The exception stacktrace was:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane expContent = new GridPane();
            expContent.setMaxWidth(Double.MAX_VALUE);
            expContent.add(label, 0, 0);
            expContent.add(textArea, 0, 1);

            alert.getDialogPane().setExpandableContent(expContent);

            alert.showAndWait();
        });
    }
}
