package com.hereisalexius.ppp.utils;

public class FieldProperty {

    public static final String DEFAULT_VALUE = "defaultValue";
    public static final String SELECTION_STYLE = "selection.style";
    public static final String SELECTION_SOURCE = "selection.source";
    public static final String SELECTION_DELIMITER = "selection.delimiter";
    public static final String SELECTION_CODES = "selection.codes";
    public static final String SELECTION_POSITIVE_VALUE = "selection.positiveValue";
    public static final String SELECTION_NEGATIVE_VALUE = "selection.negativeValue";
    public static final String SELECTION_ATTRIBUTE = "selection.attribute";
    public static final String SELECTION_RESULT = "selection.result";
    public static final String SELECTION_TABLE_KEYS = "selection.tableKeys";

    //positive and negative
    private ProductField field;

    public FieldProperty(ProductField field) {
        this.field = field;
    }

    public String getDefaultValue() {
        return AppConfig.getInstance().getProperty(field + "." + DEFAULT_VALUE);
    }

    public void setDefaultValue(String value) {
        AppConfig.getInstance().setProperty(field + "." + DEFAULT_VALUE, value);
    }

    public FieldSelectionStyle getSelectionStyle() {
        String value = AppConfig.getInstance().getProperty(field + "." + SELECTION_STYLE);
        if (value != null) {
            return FieldSelectionStyle.valueOf(value.toUpperCase());
        }
        return FieldSelectionStyle.NONE;
    }

    public void setSelectionStyle(FieldSelectionStyle value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_STYLE, value.toString());
    }

    public FieldSelectionSource getSelectionSource() {
        String value = AppConfig.getInstance().getProperty(field + "." + SELECTION_SOURCE);
        if (value != null) {
            return FieldSelectionSource.valueOf(value.toUpperCase());
        }

        return FieldSelectionSource.NONE;
    }

    public void setSelectionSource(FieldSelectionSource value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_SOURCE, value.toString());
    }

    public String getSelectionDelimiter() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_DELIMITER);
    }

    public void setSelectionDelimiter(String value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_DELIMITER, value);
    }

    public String[] getSelectionCodes() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_CODES).split(",");
    }

    public String getSelectionCodesInLine() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_CODES);
    }

    public void setSelectionCodes(String... codes) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_CODES, arrayToRow(codes));
    }

    public String getPositiveValue() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_POSITIVE_VALUE);
    }

    public void setPositiveValue(String value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_POSITIVE_VALUE, value);
    }

    public String getNegativeValue() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_NEGATIVE_VALUE);
    }

    public void setNegativeValue(String value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_NEGATIVE_VALUE, value);
    }

    public String getAttribute() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_ATTRIBUTE);
    }

    public void setAttribute(String value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_ATTRIBUTE, value);
    }

    public Result getSelectionResult() {
        String value = AppConfig.getInstance().getProperty(field + "." + SELECTION_RESULT);
        if (value != null) {
            return Result.valueOf(value.toUpperCase());
        }

        return Result.TEXT;
    }

    public void setSelectionResult(Result value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_RESULT, value.toString());
    }

    public String getSelectionTableKeys() {
        return AppConfig.getInstance().getProperty(field + "." + SELECTION_TABLE_KEYS);
    }

    public void setSelectionTableKeys(String value) {
        AppConfig.getInstance().setProperty(field + "." + SELECTION_TABLE_KEYS, value);
    }

    //peplaceable regexps
    //replace with what?
    private String arrayToRow(String[] arr) {
        String inRow = "";
        for (String s : arr) {
            inRow += "," + s;
        }
        return inRow.substring(1);
    }

}
