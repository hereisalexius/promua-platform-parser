package com.hereisalexius.ppp.utils;

public enum FieldSelectionStyle {
    NONE,
    SIMPLE_CSS_SELECTOR,
    MULTIPLY_CSS_SELECTOR,
    CONTAINS_TEXT,
    BINARY_TABLE_CSS_SELECTOR,
    VALUE_FROM_BINARY_TABLE_CSS_SELECTOR;

}
