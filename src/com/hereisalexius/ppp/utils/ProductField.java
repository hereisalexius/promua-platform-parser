package com.hereisalexius.ppp.utils;

public enum ProductField {
    CODE("1_code"),
    POSITION_TITLE("2_position_title"),
    KEYWORDS("3_keywords"),
    DESCRIPTION("4_description"),
    TYPE("5_type"),
    PRICE("6_price"),
    CURRENCY("7_currency"),
    MEASURE("8_measure"),
    MIN_ORDER_COUNT("9_minOrderCount"),
    WHOLESALE_PRICE("10_wholesalePrice"),
    MIN_WHOLESALE_COUNT("11_minWholesaleCount"),
    IMAGE_URLS("12_imageURLs"),
    STATE("13_state"),
    COUNT("14_count"),
    GROUP_NUMBER("15_groupNumber"),
    SUBSECTION_ADDRESS("16_subsectionAddress"),
    DELIVERY_POSIBILITY("17_deliveryPosibility"),
    DELIVERY_TERM("18_deliveryTerm"),
    PACKING_TYPE("19_packingType"),
    UNIQ_ID("20_uniqId"),
    ID("21_id"),
    SUBSECTON_ID("22_subsectionId"),
    GROUP_ID("23_groupId"),
    PRODUCER("24_producer"),
    GUARANTY_TERM("25_guarantyTerm"),
    PRODUCER_COUNTRY("26_producerCountry"),
    DISCOUNT("27_discount"),
    DIFFERENCE_GROUP_ID("28_differenceGroupId"),
    PRODUCER_TITLE("29_producerTitle"),
    PRODUCER_ADDRESS("30_producerAddress"),
    TOTAL_CHARACTERISTICS("31_totalCharacteristics"),
    USER_TOTAL_CHARACTERISTICS("32_userTotalCharacteristics"),
    CHARACTERISTICS("33_characteristics");

    private final String name;

    private ProductField(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
