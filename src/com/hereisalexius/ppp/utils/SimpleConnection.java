package com.hereisalexius.ppp.utils;

import java.io.IOException;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class SimpleConnection {

    public static Document connect(String url) throws IOException {

        Document doc = null;
        try {
            doc = Jsoup.connect(url)
                    .userAgent(AppConfig.getInstance().getProperty(AppConfig.PROPNAME_USER_AGENT))
                    .timeout(Integer.parseInt(AppConfig.getInstance().getProperty(AppConfig.PROPNAME_CONNECTION_TIMEOUT)))
                    .get();
            return doc;
        } catch (Exception ex) {
            ex.printStackTrace();
            AppConfig.showErrorAlert("Error Dialog",
                    "Connection Error",
                    "Please check for :"
                    + "\n\t - Internet connection"
                    + "\n\t - Captcha"
                    + "\n\t - URL validation "
                    + "\n\t - Connection properties", ex);
        }
        return doc;
    }

}
